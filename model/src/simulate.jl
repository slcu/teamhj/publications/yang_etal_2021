
# get_u0(::AbstractSimulation, model, p::Param{R4FeedbackCYT, T}) where {T} = [0, 0, round( p[:p_i]/p[:d_i]), 1]
get_u0(::AbstractSimulation, model, p) = fill(0., numspecies(model))
get_u0(sim::AbstractSimulation, p::AbstractParameter) = get_u0(sim, p.model, p)

get_u0(::Gillespie, args...) = round.(Int, get_u0(ODE(), args...))



simulate(sim, trigger, p::AbstractParameter, args...; kwargs...) = simulate(sim, trigger, p.model, p, args...; kwargs...)

function simulate(::ODE, trigger::AbstractTrigger, model::DiffEqBase.AbstractReactionNetwork, p; tspan=(0.,10.), u0 = get_u0(ODE(), model, p), callback=nothing, kwargs...)
    callback = CallbackSet(get_callback(trigger, model), callback)
    prob = ODEProblem(model, u0, tspan, deepcopy(p))
    sol = solve(prob; callback=callback, kwargs...)
    return sol
end

function simulate(::Gillespie, trigger::AbstractTrigger, model::DiffEqBase.AbstractReactionNetwork, p; tspan=(0.,10.), u0 = get_u0(Gillespie(), model, p), callback=nothing, kwargs...)
    callback = CallbackSet(get_callback(trigger, model), callback)
    prob = DiscreteProblem(u0, tspan, deepcopy(p))
    jump_prob = JumpProblem(prob, Direct(), model, save_positions=(false, false))
    sol = solve(jump_prob; callback=callback, saveat=(last(tspan) - first(tspan))/500, kwargs...)
    return sol
end

function simulate(::Ensemble, trigger::AbstractTrigger, model::DiffEqBase.AbstractReactionNetwork, p; tspan=(0.,10.), u0 = get_u0(Gillespie(), model, p), callback=nothing, trajectories=10, kwargs...)
    callback = CallbackSet(get_callback(trigger, model), callback)
    prob = DiscreteProblem(u0, tspan, deepcopy(p))
    jump_prob = JumpProblem(prob, Direct(), model, save_positions=(false, false))
    sol = solve(EnsembleProblem(jump_prob); callback=callback, saveat=(last(tspan) - first(tspan))/500, trajectories=trajectories, kwargs...)
    return sol
end

# function simulate(model::R4FeedbackCYT, p; tspan=(0.,10.), u0 = get_u0(p))
#     condition(u, t, i) = - (2 - t) * (3 - t)
#     affect!(i) = (2 <= i.t < 3 ? i.u[4] = 10 : i.u[4] = 1 )

#     cont_cb = ContinuousCallback(condition, affect!)

#     ode_prob = ODEProblem(p.model, u0, tspan, p.param)
#     ode_sol = solve(ode_prob; callback=cont_cb)
#     ode_sol
# end

function simulate(model, p; tspan=(0.,10.), u0 = get_u0(p))
    ode_prob = ODEProblem(p.model, u0, tspan, p.param)
    ode_sol = solve(ode_prob)
    ode_sol
end

