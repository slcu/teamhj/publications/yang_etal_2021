@reaction_network R4Feedback begin
    ## R4 has a basal production. Proteins are produced in the cytosol.
    p_r, 0 --> R4_c

    ## R4 degrades both in the cytosol and the nucleus.
    d_r, (R4_c, R4_n) --> 0

    ## importer has basal production and is upregulated by nuclear R4.
    p_i + hill(R4_n, v_i, k_i, n_i), 0 --> importer

    ## Degradation of the importer.
    d_i, importer --> 0

    ## Importer imports cytosolic R4. R4 export is repressed by CYT.
    (r_import*importer, r_export), R4_c ↔ R4_n
end p_r d_r p_i v_i k_i n_i d_i r_import r_export noise

function get_u0(::AbstractSimulation, model::R4Feedback, p)
    eq = fill(0., numspecies(model))
    eq[3] = p[3]/p[7]
    return eq
end
get_u0(::Gillespie, model::R4Feedback, p) = round.(Int, get_u0(ODE(), model, p))



@reaction_network R4FeedbackCYT begin
    ## R4 has a basal production. Proteins are produced in the cytosol.
    p_r, 0 --> R4_c

    ## R4 degrades both in the cytosol and the nucleus.
    d_r, (R4_c, R4_n) --> 0

    ## importer has basal production and is upregulated by nuclear R4.
    p_i + hill(R4_n, v_i, k_i, n_i), 0 --> importer

    ## Degradation of the importer.
    d_i, importer --> 0

    ## Importer imports cytosolic R4. R4 export is repressed by CYT.
    (r_import*importer, r_export/CYT), R4_c ↔ R4_n

    0, 0 --> CYT
end p_r d_r p_i v_i k_i n_i d_i r_import r_export noise

function equilibrate(model::R4FeedbackCYT, p)
    eq = fill(0., numspecies(model))
    eq[3] = p[3]/p[7]
    eq[4] = 1
    return eq
end




function add_output!(model::DiffEqBase.AbstractReactionNetwork, nr_steps)

    addparam!(model, :r)

    for i in 1:nr_steps-1
        addspecies!(model, Symbol("X_$(i)"))
        (i > 1) && addreaction!(model, :r, (Symbol("X_$(i-1)")=>1,), (Symbol("X_$(i)")=>1,))
    end
    addspecies!(model, :Output)

    addreaction!(model, :(r * $(Symbol("X_$(nr_steps-1)"))), :(0 --> Output) )
    addreaction!(model, :r, :($(Symbol("X_$(nr_steps-1)")) --> 0) )
    addreaction!(model, :r, :(Output --> 0) )
    addreaction!(model, :(r * R4_n), :(0 --> X_1))

    addodes!(model)
    addsdes!(model)
    addjumps!(model)
    return nothing
end


@reaction_network ModelCytImport begin
    ## A 5-step linear pathway supplies a nice and smooth gamma-distributed delay for the CYT peak.
    # This is just the generation of a somewhat realistic input. 
    # It should not be considered part of the model proper.
    r, X_1 --> X_2
    r, X_2 --> X_3
    r, X_3 --> X_4
    r, X_4 --> X_5
    r, X_5 --> CYT
    r_cyt, CYT --> 0
    eq_cyt * r_cyt, 0 --> CYT
    
    ## Model reactions
    # Import
    r_i * (k + CYT) * (R4_tot - N), 0 --> N
    
    # Export
    r_e, N --> 0
    
    ## specify what quantities should be considered parameters (as opposed to variables).
end r r_cyt r_i k r_e R4_tot eq_cyt

# Set default parameter values
DiffEqParameters.Param(model::ModelCytImport) = Param(model, [10.0, 10.0, 0.91, 2.3, 14.0, 1e3, 10.0])

@reaction_network ModelCytExport begin
    ## Creating the input
    r, X_1 --> X_2
    r, X_2 --> X_3
    r, X_3 --> X_4
    r, X_4 --> X_5
    r, X_5 --> CYT
    r_cyt, CYT --> 0
    eq_cyt * r_cyt, 0 --> CYT
    
    ## Model dynamics
    r_i * (R4_tot - N), 0 --> N
    r_e/(k + CYT), N --> 0
end r r_cyt r_i k r_e R4_tot eq_cyt 

# Set default parameter values
DiffEqParameters.Param(model::ModelCytExport) = Param(model, [10.0, 10.0, 0.91*10, 2.3, 14.0*10, 1e3, 10.0])


@reaction_network ModelCytProdFeedback begin
    r, X_1 --> X_2
    r, X_2 --> X_3
    r, X_3 --> X_4
    r, X_4 --> X_5
    r, X_5 --> CYT
    r_cyt, CYT --> 0
    eq_cyt * r_cyt, 0 --> CYT
    (p, d), 0 ↔ C
    d, N --> 0
    r_i * (k_i + hill(N, 1., k_n_imp, n_hill)), C --> N
    r_e/(k + CYT), N --> C
    end r r_cyt k_i r_i k_n_imp k r_e eq_cyt p d n_hill

DiffEqParameters.Param(model::ModelCytProdFeedback)= Param(model, [10.0, 10.0, 0.25, 5.0, 1800.0, 0.0, 25.0, 10.0, 1000.0, 0.3, 1.])


@reaction_network ModelCytFeedback begin
    r, X_1 --> X_2
    r, X_2 --> X_3
    r, X_3 --> X_4
    r, X_4 --> X_5
    r, X_5 --> CYT
    r_cyt, CYT --> 0
    eq_cyt * r_cyt, 0 --> CYT
    r_i * (R4_tot - N), 0 --> N
    hill(N, v_i, k_n_imp, n_hill) * (R4_tot - N), 0 --> N
    r_e/(k + CYT), N --> 0
    end r r_cyt v_i r_i k_n_imp k r_e eq_cyt R4_tot n_hill

DiffEqParameters.Param(model::ModelCytFeedback)= Param(model, [10.0, 10.0, 5., 1.0, 1800.0, 0.0, 25.0, 10.0, 1000., 1.])


@reaction_network ModelCytImportFeedback begin
    r, X_1 --> X_2
    r, X_2 --> X_3
    r, X_3 --> X_4
    r, X_4 --> X_5
    r, X_5 --> CYT
    r_cyt, CYT --> 0
    eq_cyt * r_cyt, 0 --> CYT
    r_i * (R4_tot - N) * (k + CYT), 0 --> N
    hill(N, v_i, k_n_imp, n_hill) * (R4_tot - N) * (k + CYT), 0 --> N
    r_e, N --> 0
    end r r_cyt v_i r_i k_n_imp k r_e eq_cyt R4_tot n_hill

DiffEqParameters.Param(model::ModelCytImportFeedback)= Param(model, [10.0, 10.0, 5., 1.0, 1800.0, 0.0, 25.0, 10.0, 1000., 1.])