
@recipe function f(
                   simtype::AbstractSimulation,
                   trigger::AbstractTrigger,
                   param::AbstractParameter;
                   u0 = get_u0(simtype, param.model, param),
                   tspan = (0., 10.),
                  )
    simulate(simtype, trigger, param.model, param; tspan=tspan, u0 = u0)

end


@userplot PlotTrigger

@recipe function f(h::PlotTrigger;
                   tspan = (0., 10.),
                   trajectories = 100,
                  )
    if length(h.args) == 1 && h.args[1] isa AbstractParameter
        param = h.args[1]
        model = param.model
    elseif length(h.args) == 2 && h.args[1] isa DiffEqBase.AbstractReactionNetwork
        model, param = h.args
    else 
        error("plottrigger either takes a `Param` or a model and a parameter set as an input.")
    end

    layout --> (1,2)
    xlabel --> "Time [hours]"
    ylabel --> "Molecules"
    size --> (800,400)

    for (i, trigger) in enumerate([NoTrigger(), Trigger()])
        @series begin
            subplot := 1
            color := 3 - i
            vars --> 1:2
            linestyle --> [:dash :solid]
            alpha := 1
            label --> ["C - $(typeof(trigger))" "N - $(typeof(trigger))"]

            u0 = get_u0(ODE(), model, param)
            simulate(ODE(), trigger, model, param; tspan=tspan, u0 = u0)
        end
    end

    for (i, trigger) in enumerate([NoTrigger(), Trigger()])
        @series begin
            subplot := 2
            color := 3 - i
            vars --> 2
            alpha --> 0.4

            u0 = get_u0(Ensemble(), model, param)
            simulate(Ensemble(), trigger, model, param; tspan=tspan, u0 = u0, trajectories=trajectories)
        end
    end

end


@userplot PlotSensitivity

@recipe function f(h::PlotSensitivity;
                   tspan = (0., 10.),
                   vars = 1:2,
                  )
    if length(h.args) == 1 && h.args[1] isa AbstractParameter
        param = h.args[1]
        model = param.model
    elseif length(h.args) == 2 && h.args[1] isa DiffEqBase.AbstractReactionNetwork
        model, param = h.args
    else 
        error("plotsensitivity either takes a `Param` or a model and a parameter set as an input.")
    end
    u0 = get_u0(ODE(), model, param)
    prob = ODELocalSensitivityProblem(model, u0, (0., 10.), param)
    sol = solve(prob)

    layout --> (ceil(Int, (length(param) + 1) / 3), 3)
    size --> (800, 400)
    legend --> false
    xlabel --> "Time [hours]"
    link --> :x
    for i in 1:numparams(model)+1
        @series begin
            subplot := i
            vars --> (i - 1) .* numspecies(model) .+ vars
            label --> hcat("Cytosolic", "Nuclear", fill("", length(vars)-2)...)
            color --> hcat(2, 1, fill(1, length(vars)-2)...)
            alpha --> hcat(1, 1, fill(0.2, length(vars)-2)...)
            title --> (i == 1 ? "Simulation" : latexify(params(model)[i-1]))
            sol
        end
    end
end
