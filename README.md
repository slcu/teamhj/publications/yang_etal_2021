## Introduction

This repository holds files for the analysis scripts and model simulations for the manuscript

Weibing Yang, Sandra Cortijo, Niklas Korsbo, Pawel Roszak, Katharina Schiessl, Aram Gurzadyan, Raymond Wightman, Henrik Jönsson, Elliot Meyerowitz (2021)
<i>Molecular mechanism of cytokinin-activated cell division in Arabidopsis</i> (in revision)

## Data

All original confocal imaging stacks are provided via the University of Cambridge Open Data Repository (link).

## This repository

The main files provided in this repository are distributed in the folders:


<tt>model</tt> <a href="https://julialang.org">Julia</a> files used to generate the model simulations 

<tt>Cell quantification</tt> Scripts and protocol for doing signal quantification using <a href="https://gitlab.com/slcu/teamhj/costanza">Costanza</a> for nuclear segmentation.

Further information is given in respective folder.

## Contact

For further information or questions, please contact

wbyang@cemps.ac.cn

henrik.jonsson@slcu.cam.ac.uk

meyerow@caltech.edu
